namespace BH_TEngine 
{
  public class Buchung 
  {
    public string Aufgabe {get; set;}
    public string Beschreibung {get;set;}
    public string Datum {get;set;}
    public float Dauer { get; set; }
  }
}