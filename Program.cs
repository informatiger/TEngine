﻿using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;
using TemplateEngine.Docx;

namespace BH_TEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            TableContent table = new TableContent("Tabelle");
            File.Delete("outDoc.docx");
            File.Copy("bh-template.docx", "outDoc.docx");

            JsonConvert.DeserializeObject<List<Buchung>>(File.ReadAllText("buchungen.json")).ForEach(buchung => {
                Console.WriteLine($"{buchung.Aufgabe} - {buchung.Beschreibung}: {buchung.Dauer}");

                table.AddRow(
                    new FieldContent("Beschreibung", buchung.Beschreibung),
                    new FieldContent("Zeit", String.Format("{0:0.00}", buchung.Dauer).Replace(".", ","))
                );
            });

            if (table != null)
            {
                using (var tProcessor = new TemplateProcessor("outDoc.docx").SetRemoveContentControls(true))
                {
                    tProcessor.FillContent(new Content(table));
                    tProcessor.SaveChanges();
                }
            }
        }
    }
}
